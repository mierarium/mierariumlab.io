/* ==========================================================================
   jQuery plugin settings and other scripts
   ========================================================================== */


$(document).ready(function() {

    $.fn.invisible = function() {
        return this.each(function() {
            $(this).css("visibility", "hidden");
        });
    };
    $.fn.visible = function() {
        return this.each(function() {
            $(this).css("visibility", "visible");
        });
    };

  // Sticky footer
  var bumpIt = function() {
      $("body").css("margin-bottom", $(".page__footer").outerHeight(true));
    },
    didResize = false;

  bumpIt();

  $(window).resize(function() {
    didResize = true;
  });
  setInterval(function() {
    if (didResize) {
      didResize = false;
      bumpIt();
    }
  }, 250);

  // FitVids init
  $("#main").fitVids();

  // Sticky sidebar
  var stickySideBar = function() {
    var show =
      $(".author__urls-wrapper button").length === 0
        ? $(window).width() > 1024 // width should match $large Sass variable
        : !$(".author__urls-wrapper button").is(":visible");
    if (show) {
      // fix
      $(".sidebar").addClass("sticky");
    } else {
      // unfix
      $(".sidebar").removeClass("sticky");
    }
  };

  stickySideBar();

  $(window).resize(function() {
    stickySideBar();
  });

  // Follow menu drop down
  $(".author__urls-wrapper button").unbind("click");
  $(".author__urls-wrapper button").on("click", function() {
    $(".author__urls").toggleClass("is--visible");
    $(".author__urls-wrapper button").toggleClass("open");
  });

  // Search toggle
  $(".search__toggle").unbind("click");
  $(".search__toggle").on("click", function() {
    $(".search-content").toggleClass("is--visible");
    $(".initial-content").toggleClass("is--hidden");
    // set focus on input
    setTimeout(function() {
      $(".search-content input").focus();
    }, 400);
  });

  // init smooth scroll
  $("a").smoothScroll({ offset: -20 });

  // add lightbox class to all image links
  $(
    "a[href$='.jpg'],a[href$='.jpeg'],a[href$='.JPG'],a[href$='.png'],a[href$='.gif']"
  ).addClass("image-popup");

  // Magnific-Popup options
  $(".image-popup").magnificPopup({
     disableOn: function() {
       if( $(window).width() < 1024 ) {
         return false;
       }
       return true;
     },
	preventPropagation:true,
    type: "image",
    tLoading: "Loading image #%curr%...",
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">Image #%curr%</a> could not be loaded.'
    },
    removalDelay: 500, // Delay in milliseconds before popup is removed
    // Class that is added to body when popup is open.
    // make it unique to apply your CSS animations just to this exact popup
    mainClass: "mfp-zoom-in",
    callbacks: {
      beforeOpen: function() {
        // just a hack that adds mfp-anim class to markup
        this.st.image.markup = this.st.image.markup.replace(
          "mfp-figure",
          "mfp-figure mfp-with-anim"
        );
      }
    },
    closeOnContentClick: true,
    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
  });
  
  // When the user scrolls the page, execute siteOnScroll
	window.onscroll = function() {siteOnScroll()};

  // Get the offset position of the navbar
  var stickyTop = $("#main").offset().top/2;
  var paralaxImg = $(".parallax").offset();
  $(".parallax").css('background-position', "center 0px");

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function siteOnScroll() {
    if (window.pageYOffset > stickyTop) {
      //$(".masthead__inner-wrap").addClass("fixed-header");
	  $(".site-title-mast").visible();
	  $(".logo-header").css('opacity', '0');
	  $(".logo-header").invisible();
    } else {
      //$(".masthead__inner-wrap").removeClass("fixed-header");
	  $(".site-title-mast").invisible();
	  $(".logo-header").css('opacity', ''+((stickyTop - window.pageYOffset) / stickyTop));
	  $(".logo-header").visible();
    }

    $(".parallax").css('background-position', "center "+(-4 * window.pageYOffset)+"px");
  }

  
});
