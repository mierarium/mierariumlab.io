---
title: "Pagină inexistentă"
header:
  overlay_image: /assets/images/hdr.jpg
layout: shop-category
sitemap: false
permalink: /404.html
author_profile: true
excerpt: "Ne pare rău, pagina pe care încercați să o accesați nu există."
sidebarshop:
  nav: "shopnav"
classes: wide

categorii:
  - url: /shop/miere-vie/
    image_path: /assets/images/shop/miere-vie/miere_vie_th.jpg
    alt: "miere vie"
    title: "MIERE VIE"
  - url: /shop/cadouri/
    image_path: /assets/images/shop/cadouri/cadouri_th.jpg
    alt: "cadouri"
    title: "CADOURI"
  - url: /shop/cadouri/gherghina
    image_path: /assets/images/shop/cadouri/gherghina/gherghina_th.jpg
    alt: "Cadou Gherghina"
    title: "CADOU GHERGHINA"


---

Navigați în [Shop](/shop/), [Jurnal](/jurnal/) sau 
<button style="margin:0" class="search__toggle" type="button">
    <svg class="icon" width="16" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.99 16">
      <path d="M15.5,13.12L13.19,10.8a1.69,1.69,0,0,0-1.28-.55l-0.06-.06A6.5,6.5,0,0,0,5.77,0,6.5,6.5,0,0,0,2.46,11.59a6.47,6.47,0,0,0,7.74.26l0.05,0.05a1.65,1.65,0,0,0,.5,1.24l2.38,2.38A1.68,1.68,0,0,0,15.5,13.12ZM6.4,2A4.41,4.41,0,1,1,2,6.4,4.43,4.43,0,0,1,6.4,2Z" transform="translate(-.01)"></path>
    </svg>
</button>


<h4 class="hr-line"><span>Categorii</span></h4>

{% include category-gallery id="categorii" %}

<h4 class="hr-line"><span>In stoc</span></h4>

{% include category-stock-gallery %}