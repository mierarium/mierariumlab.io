---
title: "Comandă"
permalink: /checkout/
header:
  overlay_image: /assets/images/header_shop.jpg
  no-caption: "Imagine: Ambalarea mierii de fâneață **mierarium**"
breadcrumbs: true
breadcrumb_home_label : "Comandă"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
layout: checkout
classes: wide
highlighter: none
excerpt: "📦 Livrăm personal în București, Craiova și Constanța sau prin curier în țară."
search: false
---

📦 Livrăm personal în București, Craiova și Constanța sau prin curier în țară.


<div id="id-basket-head"></div>
<div id="id-checkout-basket-body"></div>
<div id="id-div-hidden-input"></div>
<!-- <div class="shop-product-order-btn shop-btn-order-copy noselect">Copiaza text</div> -->
<div class="shop-product-order-btn shop-btn-order-empty noselect">Golește coșul</div>  
<div class="noselect" style="display:inline" id="id-empty-basket-confirm"></div>
<br/>
<form id="id-form-ckeckout" class="gform pure-form pure-form-stacked" method="POST"   
  action="https://script.google.com/macros/s/AKfycbw7tK7pw55fB5a_EI8Tope194ZWRf_Mi_qF62XUEw/exec">

    <div class="form-elements">

	  <fieldset style="margin-right:2%; float:left; width: 49%;" class="pure-group">
        <label for="p">Prenume </label>
        <input required id="p" name="p" required />
      </fieldset>
	  
      <fieldset class="pure-group">
        <label for="n">Nume </label>
        <input required id="n" name="n" required />
      </fieldset>
	  
	  <input id="id-shop-form-cmd" name="x" type="hidden" />
	  <input id="id-shop-form-cmd-nice" name="y" type="hidden" /> 

      <fieldset class="pure-group">
        <label for="a">Adresa</label>
        <input id="a" name="a" value=""
        required placeholder="Stradă, număr, cod postal"/>
		<input id="a2" name="a2" value=""
        placeholder="Apartament, etaj, clădire, etc (optional)"/>
      </fieldset>

      <fieldset class="pure-group">
        <label for="o">Oras </label>
        <input required id="o" name="o" required />
      </fieldset>
	  
      <fieldset class="pure-group">
        <label for="j">Județ / sector </label>
        <input required id="j" name="j" required />
      </fieldset>
	  
      <fieldset class="pure-group">
        <label for="t">Telefon </label>
        <input required id="t" name="t" required />
      </fieldset>

      <fieldset class="pure-group">
        <label for="e">Adresă email</label>
        <input id="e" name="e" type="email" value=""
        required placeholder="adresa@email.com"/>
      </fieldset>

      <input id="h" type="hidden" name="h" value="" />

      <button class="button-success pure-button button-xlarge">
        <i class="fa fa-paper-plane"></i>&nbsp;Comandă</button>

    </div>

    <div class="thankyou_message" style="display:none;">
      <h2>Vă mulțumim pentru comandă!
        Vom reveni cu un email în curand!</h2>
    </div>

  </form>

Pentru alte informații, ne găsiți pe Facebook Messenger sau WhatsApp.

<div class="shop-product-order-btn shop-btn-order-fb noselect"><i class="fa-brands fa-facebook-messenger"></i> Messenger</div> 
<div class="shop-product-order-btn shop-btn-order-wa noselect"><i class="fa-brands fa-whatsapp"></i> WhatsApp</div> 

<hr/>
<b>mierarium</b> este o mică stupină de familie din județul Dolj unde descoperim în fiecare zi frumusețea și simplitatea lucrurilor din jurul nostru.<br/> 

<h4 class="hr-line"><span>In stoc</span></h4>

{% include category-stock-gallery %}