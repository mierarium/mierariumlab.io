---
title: "Shop"
excerpt: >
  <b>mierarium</b> este o mică stupină de familie din județul Dolj unde descoperim în fiecare zi frumusețea și simplitatea lucrurilor din jurul nostru.
permalink: /shop/
layout: home-shop

header:
  overlay_image: /assets/images/shop/header_shop.jpg

author_profile: false
sidebarshop:
  nav: "shopnav"

classes: wide
searchme: true


categorii:
  - url: /shop/miere-vie/
    image_path: /assets/images/shop/miere-vie/miere_vie_th.jpg
    alt: "miere vie"
    title: "MIERE VIE"
  - url: /shop/cadouri/
    image_path: /assets/images/shop/cadouri/cadouri_th.jpg
    alt: "cadouri"
    title: "CADOURI"
  - url: /shop/mierarium-labs/
    image_path: /assets/images/shop/mierarium-labs/mierariumlabs_th.jpg
    alt: "laborator"
    title: "LABORATOR"

produse:
  - url: /shop/miere-vie/salcam/
    image_path: /assets/images/shop/miere-vie/salcam/msalcam_21_400.jpg
    alt: "Miere vie salcam"
    title: "MIERE SALCAM"
  - url: /shop/miere-vie/floarea-soarelui/
    image_path: /assets/images/shop/miere-vie/floarea-soarelui/m_fs_21_400.jpg
    alt: "Miere vie floarea soarelui"
    title: "MIERE FLOAREA SOARELUI"
  - url: /shop/cadouri/gherghina
    image_path: /assets/images/shop/cadouri/gherghina/gherghina_th.jpg
    alt: "Cadou Gherghina"
    title: "CADOU GHERGHINA"

---

<div class="page_shop_inner-wrap">

<h4 class="hr-line"><span>Categorii</span></h4>

{% include category-gallery id="categorii" %}

<h4 class="hr-line"><span>Produse recomandate</span></h4>

{% include category-gallery id="produse" %}

<h4 class="hr-line"><span>In stoc</span></h4>

{% include category-stock-gallery %}

</div>