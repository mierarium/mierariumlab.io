---
layout: single
permalink: /
header:
#  overlay_color: "#5e616c" 
  video:
    id: bjiHWbQO2c0?rel=0&mute=1&autoplay=1&controls=1&modestbranding=1
    provider: youtube
excerpt: >
  <b>mierarium</b> este o mică stupină de familie din județul Dolj unde descoperim în fiecare zi frumusețea și simplitatea lucrurilor din jurul nostru.<br/> <br/>folosim metode tradiționale obținând <b>mierea vie</b>, acea miere <i>netratată termic</i>, cu <i>dulceața</i>, <i>vitaminele</i> și <i>mineralele</i> atât de benefice traiului omului
author_profile: true
classes: wide
gallery:
  - url: /assets/posts/home/DSC06999.jpg
    image_path: /assets/posts/home/DSC06999-th.jpg
    alt: "mierarium in aprilie"
    title: "Aprilie in mierarium"
  - url: /assets/posts/home/DSC06983.jpg
    image_path: /assets/posts/home/DSC06983-th.jpg
    alt: "mierarium in aprilie"
    title: "Aprilie in mierarium"
  - url: /assets/posts/home/DSC07006.jpg
    image_path: /assets/posts/home/DSC07006-th.jpg
    alt: "mierarium in aprilie"
    title: "Aprilie in mierarium"
  - url: /assets/posts/home/DSC07082.jpg
    image_path: /assets/posts/home/DSC07082-th.jpg
    alt: "mierarium in aprilie"
    title: "Aprilie in mierarium"
  - url: /assets/posts/home/DSC07146.jpg
    image_path: /assets/posts/home/DSC07146-th.jpg
    alt: "mierarium in aprilie"
    title: "Aprilie in mierarium"
  - url: /assets/posts/home/DSC07240.jpg
    image_path: /assets/posts/home/DSC07240-th.jpg
    alt: "mierarium in aprilie"
    title: "Aprilie in mierarium"    
---

{% include gallery caption="Primăvara în **mierarium**" %}