---
title: Mărțisor Lavandă
layout: single-shop-category
permalink: /shop/cadouri/mt-lavanda/
shop-category: mt-lavanda
header:
  overlay_image: /assets/images/shop/cadouri/mt-lavanda/header_mt_lavanda.jpg
  teaser: /assets/images/shop/cadouri/mt-lavanda/header_mt_lavanda.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
classes: wide
excerpt: "Mărțisoare cu ulei esențial de lavandă."
searchme: true
---

🌸 În așteptarea păcii și a primăverii, am pregătit mărțisoare cu ulei esențial de lavandă.

⚗️ În vară am obținut uleiul esențial de lavandă din 14 distilări. Nota distinctivă o dau florile culese târziu, după ce au fost vizitate de albine și fluturi, cu uleiurile bine definite, ajunse la maturitate. Timp de două săptămâni, în fiecare zi am ales cele mai potrivite 10 tufe de flori pentru a le distila în micul nostru alambic de cupru cu apa unui izvor vechi captat în 1927 aflat în satul vecin.

🔥 Alambicul l-am așezat pe soba bunicilor și focul l-am făcut cu surcele strânse la tăiatul viței de vie din primăvară.

🧪 În 10ml este concentrată esența a cinci tufe de flori de lavandă.

✉️ Ne puteți lăsa un comentariu sau mesaj pentru comandă.

💡 Noi folosim uleiul esențial:
① ca parfum pe haine sau piele,
② în difuzorul de uleiuri esențiale pentru a ne liniști după zilele stresante,
③ în aromarea dulciurilor, ultima oară am aromat tiramisu cu o picătură!
④ pentru inhalații.