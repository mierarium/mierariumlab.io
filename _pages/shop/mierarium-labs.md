---
title: Mierarium Labs
layout: home-shop
taxonomy: mierarium-labs
shop_category: shop-main-category
permalink: /shop/mierarium-labs/
entries_layout: grid
classes: wide
header:
  teaser: /assets/images/shop/mierarium-labs/mierariumlabs_th.jpg
  overlay_image: /assets/images/shop/mierarium-labs/mierarium-labs_header.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
excerpt: "Mierea direct extrasă din stup, neprocesată, cu arome complexe de flori, ceară si propolis."
searchme: true
sidebarshop:
  nav: "shopnav"
---

Mierea vie este <i>netratată termic</i>, cu <i>dulceața</i>, <i>vitaminele</i> și <i>mineralele</i> benefice traiului omului.

Deoarece nu este încalzită în momentul extragerii și fiind filtrată gravitațional prin site simple din inox alimentar, poate conține particule fine de ceară și polen.

<h4 class="hr-line"><span>{{ page.title }}</span></h4>

{% include category-shop-gallery %}