---
title: Căpăceală
layout: single-shop-category
permalink: /shop/mierarium-labs/capaceala/
shop-category: capaceala_2021
header:
  overlay_image: /assets/images/shop/mierarium-labs/capaceala/header_capaceala.jpg
  teaser: /assets/images/shop/mierarium-labs/capaceala/header_capaceala_th.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
classes: wide
excerpt: "căpăceala este pojghița fină(căpăcelul) de ceară și propolis, pieptănată de pe faguri, cu care albina sigilează mierea.⁣"
searchme: true
---

c͟ă͟p͟ă͟c͟e͟a͟l͟a͟ este pojghița fină(căpăcelul) de ceară și propolis, pieptănată de pe faguri, cu care albina sigilează mierea.⁣
⁣<br/>
p͟r͟o͟p͟o͟l͟i͟s͟u͟l͟ este un amestec de rășini secretate de arbori. Albinele îl strâng pentru proprietățile sale puternic antiseptice.⁣
⁣
c͟ă͟p͟ă͟c͟e͟a͟l͟a͟:⁣<br/>
‣ calmează tusea și durerile în gât⁣<br/>
‣ ajută la menținerea unor gingii sănătoase prin igienizarea cavității bucale⁣<br/>
‣ ameliorează sinuzita<br/>
‣ stimulează sistemul imunitar⁣<br/>

🐝 Mierea artizanală vie creată de mierarium nu este încălzită, o filtrăm prin curgere liberă în site fine de inox iar polenul încă se regăsește în miere, ceea ce sporește calitatile nutriționale dar grabeste procesul de cristalizare.

🏭 Marii procesatori de miere încălzesc mierea la 70-80 C pentru a scădea vascozitatea și a o putea filtra suplimentar sub presiune, acest lucru prelungește timpul în care mierea rămâne cristalizata la raft. Din păcate acest proces distruge enzimele, aminoacizii, vitaminele și o parte din arome iar cantitatea de polen din miere este redusă.

✔️ În realitate mierea cristalizată este o sursa mai buna de nutrienți, s-a demonstrat că o linguriță de miere cristalizată ținută sub limba permite o absorbție mai buna a unor nutrienți prin acțiunea enzimelor din salivă.

🍯 Fiecare miere este unică și irepetabilă, este o poveste, o istorie a florilor care au fost înflorite, a florilor care au fost găsite! Ce flori, ce
 ploi, ce seve, ce albine, toate se schimbă și se combină, iar amintirea vremurilor rămâne în lingurița cu miere.

<br/>
Recomandăm păstrarea mierii în recipiente din sticlă închise ermetic, ferită de razele directe alea soarelui, la o temperatură de peste 18°C.
<br/>
Orice sortiment de miere naturală va cristaliza în timp, în funcție de temperatura de pastrare și conținutul de glucoză al sortimentului. 
<br/>
Pentru o decristalizare eficientă și corectă, cu păstrarea proprietaților(arome, enzime, vitamine), recomandăm încalzirea apei la maxim 40°C, oprirea focului, introducerea borcanului în recipientul cu apă încalzita și agitarea mierii din borcan cu o linguriță pentru a omogeniza temperatura în interiorul borcanului. 