---
title: Cadouri
layout: home-shop
taxonomy: cadouri
shop_category: shop-main-category
permalink: /shop/cadouri/
entries_layout: grid
classes: wide
header:
  teaser: /assets/images/shop/cadouri/cadouri_th.jpg
  overlay_image: /assets/images/shop/cadouri/cadouri_header.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
excerpt: "Mierea direct extrasă din stup, neprocesată, cu arome complexe de flori, ceară si propolis."
searchme: true
sidebarshop:
  nav: "shopnav"
---

Mierea vie este <i>netratată termic</i>, cu <i>dulceața</i>, <i>vitaminele</i> și <i>mineralele</i> benefice traiului omului.

Deoarece nu este încalzită în momentul extragerii și fiind filtrată gravitațional prin site simple din inox alimentar, poate conține particule fine de ceară și polen.

<h4 class="hr-line"><span>{{ page.title }}</span></h4>

{% include category-shop-gallery %}