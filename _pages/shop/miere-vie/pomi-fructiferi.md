---
title: Pomi Fructiferi
layout: single-shop-category
permalink: /shop/miere-vie/pomi-fructiferi/
shop_category: miere-vie
header:
  overlay_image: /assets/images/header_pf.jpg
  teaser: /assets/images/header_pf_thumb.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
classes: wide
shop-category: miere-pomi-fructiferi
excerpt: "Mierea de pomi fructiferi este strânsa din livezile de pe dealuri (corcodusi, pruni, ciresi, caisi, meri) si din arbustii spontani care infloresc în aprilie-mai (porumbar, maces, paducel). "
searchme: true
---

Mierea de pomi fructiferi este strânsa de albine în perioada aprilie-mai din livezile de pe dealuri (corcodusi, pruni, ciresi, caisi, meri) si din arbustii spontani care infloresc în aceasta perioada (porumbar, maces, paducel). 

Este o miere foarte parfumata, de culoare galben deschis, dupa o perioada de cateva luni cristalizeaza fin, cremos. 

Nu se obtine în fiecare an, pentru a putea recolta aceasta minune trebuie sa avem o primavara timpurie si o luna aprilie calduroasa fara prea multe ploi sau cu ploi nocturne.

Recomandăm păstrarea mierii în recipiente din sticlă închise ermetic, ferită de razele directe alea soarelui, la o temperatură de peste 18°C.

Orice sortiment de miere naturală va cristaliza în timp, în funcție de temperatura de pastrare și conținutul de glucoză al sortimentului. 

Pentru o decristalizare eficientă și corectă, cu păstrarea proprietaților(arome, enzime, vitamine), recomandăm încalzirea apei la maxim 40°C, oprirea focului, introducerea borcanului în recipientul cu apă încalzita și agitarea mierii din borcan cu o linguriță pentru a omogeniza temperatura în interiorul borcanului. 


