---
title: Fâneață
layout: single-shop-category
permalink: /shop/miere-vie/faneata/
shop-category: miere-faneata
header:
  overlay_image: /assets/images/shop/miere-vie/faneata/header_faneata.jpg
  teaser: /assets/images/shop/miere-vie/faneata/header_fan_th.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
classes: wide
excerpt: "Mierea de faneata este cea mai complexa si variaza de la an la an. Contine nectarul florilor de mai-iunie - cimbrisor, menta, trifoi, salvie, ..."
searchme: true
---

🌼 mierea de fâneață iunie (cristalizată fin) - rezultată din nectarul mai multor flori de pășune care înfloresc la noi în mai-iunie: cimbrișor, mentă, facelia, sulfină, trifoi, lucernă, măceș, ghizdei, murăriș sălbatic, salvie...

<br/>
Recomandăm păstrarea mierii în recipiente din sticlă închise ermetic, ferită de razele directe alea soarelui, la o temperatură de peste 18°C.
<br/>
Orice sortiment de miere naturală va cristaliza în timp, în funcție de temperatura de pastrare și conținutul de glucoză al sortimentului. 
<br/>
Pentru o decristalizare eficientă și corectă, cu păstrarea proprietaților(arome, enzime, vitamine), recomandăm încalzirea apei la maxim 40°C, oprirea focului, introducerea borcanului în recipientul cu apă încalzita și agitarea mierii din borcan cu o linguriță pentru a omogeniza temperatura în interiorul borcanului. 