---
title: Salcâm
layout: single-shop-category
permalink: /shop/miere-vie/salcam/
shop_category: miere-vie
header:
  overlay_image: /assets/images/header_sal.jpg
  teaser: /assets/images/header_sal_thumb.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
classes: wide
shop-category: miere-salcam-2021
excerpt: "La inceputul lunii mai in aer pluteste mireasma salcamilor, albinele strang aceasta miere iar in iunie este gata si poate fi extrasa."
searchme: true
---

🍯Mierea de salcâm 2021, un sortiment fin, de culoare galben-solar, floral, ce va rămâne în stare lichidă o perioadă îndelungată datorită conținutului ridicat de fructoză.

§ Gustul: floral, ușor, suav

§ Parfum: flori de salcâm

§ Textura: fină

§ Vascozitate: medie

Recomandăm păstrarea mierii în recipiente din sticlă închise ermetic, ferită de razele directe alea soarelui, la o temperatură de peste 18°C.

Orice sortiment de miere naturală va cristaliza în timp, în funcție de temperatura de pastrare și conținutul de glucoză al sortimentului. 

Pentru o decristalizare eficientă și corectă, cu păstrarea proprietaților(arome, enzime, vitamine), recomandăm încalzirea apei la maxim 40°C, oprirea focului, introducerea borcanului în recipientul cu apă încalzita și agitarea mierii din borcan cu o linguriță pentru a omogeniza temperatura în interiorul borcanului. 