---
title: Păducel
layout: single-shop-category
permalink: /shop/miere-vie/paducel/
shop-category: miere-paducel
header:
  overlay_image: /assets/images/shop/miere-vie/paducel/header_paducel.jpg
  teaser: /assets/images/shop/miere-vie/paducel/header_pad_th.jpg
breadcrumbs: true
breadcrumb_home_label : "Shop"
breadcrumb_separator  : ">"
author_profile: false
sidebarshop:
  nav: "shopnav"
classes: wide
excerpt: "Mierea de păducel - mai 2021. Este o miere rară se obține odată la câțiva ani în anumite condiții de vreme."
searchme: true
---

Mierea de păducel - mai 2021. Este o miere rară se obține odată la câțiva ani în anumite condiții de vreme.

Energizantă, cu proprietăți antiseptice și antioxidante, cunoscută pentru beneficiile cardiovasculare.

§ Gustul: floral, ușor, suav.<br/>
§ Parfum: păducel, flori de primăvară<br/>
§ Aftertaste: migdale, ușor amar<br/>
§ Textura : fină<br/>
§ Vâscozitate : mare<br/>
§ Ediție limitată : 60 de borcane/400gr<br/>

Așteptam mierea de primăvară de trei ani, rar putem obține miere înainte de salcâm datorită vremii capricioase.

În 2018 am extras miere de pomi fructiferi prun, măr și cireș, in 2021 salcâmul a întârziat iar dealurile au fost ca niciodată pline de păducel înflorit.

Fiecare miere este unică și irepetabilă, este o poveste, o istorie a florilor care au fost inlorite, a florilor care au fost găsite! Ce flori, ce ploi, ce seve, ce albine, acestea toate se schimbă și se combină iar surpriza cea mare este la degustarea "ceremonială" din dimineața următoare extragerii.

<br/>
Recomandăm păstrarea mierii în recipiente din sticlă închise ermetic, ferită de razele directe alea soarelui, la o temperatură de peste 18°C.
<br/>
Orice sortiment de miere naturală va cristaliza în timp, în funcție de temperatura de pastrare și conținutul de glucoză al sortimentului. 
<br/>
Pentru o decristalizare eficientă și corectă, cu păstrarea proprietaților(arome, enzime, vitamine), recomandăm încalzirea apei la maxim 40°C, oprirea focului, introducerea borcanului în recipientul cu apă încalzita și agitarea mierii din borcan cu o linguriță pentru a omogeniza temperatura în interiorul borcanului. 